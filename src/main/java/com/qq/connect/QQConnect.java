package com.qq.connect;

import com.qq.connect.utils.QQConnectConfig;
import com.qq.connect.utils.http.HttpClient;

import javax.servlet.ServletRequest;
import java.io.Serializable;

public class QQConnect
        implements Serializable {
    private static final long serialVersionUID = 2403532632395197292L;
    protected HttpClient client = new HttpClient();
    protected String keyPre="";

    protected QQConnect() { }

    protected QQConnect(ServletRequest request) {
        this.keyPre = QQConnectConfig.getPreKey(request);
    }

    protected QQConnect(String token, String openID) {
        this.client.setToken(token);
        this.client.setOpenID(openID);
    }

    protected void setToken(String token) {
        this.client.setToken(token);
    }

    protected void setOpenID(String openID) {
        this.client.setOpenID(openID);
    }

    protected void setKeyPre(String keyPre){
        this.keyPre = keyPre;
    }
}
