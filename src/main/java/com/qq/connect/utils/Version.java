package com.qq.connect.utils;

public class Version {
    private static final String TITLE = "qq_connect_sdk";
    private static String VERSION = "2.0.0.0";

    static {
        try {
            VERSION = QQConnectConfig.getValue("version");
        } catch (Exception e) {
            VERSION = "2.0.0.0";
        }
        if (VERSION.equals("")) {
            VERSION = "2.0.0.0";
        }
    }

    public static String getVersion() {
        return VERSION;
    }

    public String toString() {
        return "qq_connect_sdk " + VERSION;
    }
}
