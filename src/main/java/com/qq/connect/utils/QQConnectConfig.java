package com.qq.connect.utils;

import javax.servlet.ServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class QQConnectConfig {
    private static Properties props = new Properties();

    static {
        try {
            props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("qqconnectconfig.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getValue(String key) {
        return props.getProperty(key);
    }

    /**
     * 根据域名获取指定配置
     * */
    public static String getValue(ServletRequest request, String key){
        return props.getProperty(getPreKey(request)+key);
    }

    /**
     * 获取配置文件键值域名前缀
     * */
    public static String getPreKey(ServletRequest request){
        String pre = StringUtils.getDomain(request.getServerName());
        pre = StringUtils.toCamelhumpStyle(pre,".");
        return pre+"_";
    }

    public static void updateProperties(String key, String value) {
        props.setProperty(key, value);
    }
}



