package com.qq.connect.javabeans.weibo;

import com.qq.connect.QQConnectException;
import com.qq.connect.QQConnectResponse;
import com.qq.connect.javabeans.Avatar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class FansIdolsBean
        extends QQConnectResponse
        implements Serializable {
    private static final long serialVersionUID = 4742904673643859727L;
    private int ret = 0;
    private int errcode = 0;
    private String msg = "";
    private int curNum = 0;
    private boolean next = false;
    private long timestamp = 0L;
    private int nextStartPos = 0;
    private ArrayList<SingleFanIdolBean> fanIdols = new ArrayList();

    public FansIdolsBean(JSONObject json)
            throws QQConnectException {
        init(json);
    }

    public ArrayList<SingleFanIdolBean> getFanIdols() {
        return this.fanIdols;
    }

    public int getNextStartPos() {
        return this.nextStartPos;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public boolean hasNext() {
        return this.next;
    }

    public int getCurNum() {
        return this.curNum;
    }

    public int getRet() {
        return this.ret;
    }

    public int getErrcode() {
        return this.errcode;
    }

    public String getMsg() {
        return this.msg;
    }

    private void init(JSONObject json)
            throws QQConnectException {
        if (json != null) {
            try {
                this.ret = json.getInt("ret");
                if (0 != this.ret) {
                    this.msg = json.getString("msg");
                    this.errcode = json.getInt("errcode");
                } else {
                    this.msg = json.getString("msg");
                    this.errcode = 0;
                    String dataString = json.getString("data");
                    if ((dataString != null) && (!dataString.equals("")) && (!dataString.equals("null"))) {
                        JSONObject jo = json.getJSONObject("data");
                        this.curNum = jo.getInt("curnum");
                        this.next = (jo.getInt("hasnext") == 0);
                        this.timestamp = jo.getLong("timestamp");
                        this.nextStartPos = jo.getInt("nextstartpos");

                        String infoString = jo.getString("info");
                        if ((infoString != null) && (!infoString.equals("")) && (!infoString.equals("null"))) {
                            JSONArray infoArray = jo.getJSONArray("info");
                            int i = 0;
                            for (int j = infoArray.length(); i < j; i++) {
                                JSONObject singleInfo = infoArray.getJSONObject(i);

                                String tagsString = singleInfo.getString("tag");
                                ArrayList<Tag> tags = new ArrayList();
                                if ((tagsString != null) && (!tagsString.equals("")) && (!tagsString.equals("null"))) {
                                    JSONArray tagsJA = singleInfo.getJSONArray("tag");
                                    int k = 0;
                                    for (int p = tagsJA.length(); k < p; k++) {
                                        JSONObject tagJO = tagsJA.getJSONObject(k);
                                        tags.add(new Tag(tagJO.getString("id"), tagJO.getString("name")));
                                    }
                                }
                                JSONObject tweetInfo = null;
                                String tweetInfoString = singleInfo.getString("tweet");
                                if ((tweetInfoString != null) && (!tweetInfoString.equals("")) && (!tweetInfoString.equals("null"))) {
                                    JSONArray tweetInfos = singleInfo.getJSONArray("tweet");
                                    tweetInfo = tweetInfos.getJSONObject(0);
                                }
                                this.fanIdols.add(new SingleFanIdolBean(singleInfo.getString("city_code"), singleInfo.getString("province_code"), singleInfo.getString("country_code"), singleInfo.getString("location"), singleInfo.getInt("fansnum"), new Avatar(singleInfo.getString("head")), singleInfo.getInt("idolnum"), singleInfo.getBoolean("isfans"), singleInfo.getBoolean("isidol"), singleInfo.getInt("isrealname") == 1, singleInfo.getInt("vip") == 1, singleInfo.getString("name"), singleInfo.getString("nick"), singleInfo.getString("openid"), singleInfo.getInt("sex") == 1 ? "男" : "女", tags, new SimpleTweetInfo(tweetInfo.getString("from"), tweetInfo.getString("id"), tweetInfo.getString("text"), tweetInfo.getLong("timestamp"))));
                            }
                        }
                        jo = null;
                    }
                }
            } catch (JSONException jsone) {
                throw new QQConnectException(jsone.getMessage() + ":" + json.toString(), jsone);
            }
        }
    }
}



