package com.qq.connect.javabeans.weibo;

import java.io.Serializable;

public class PrivateFlag
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private int privateFlag = 0;

    public PrivateFlag(int i) {
        this.privateFlag = i;
    }

    public int getPrivateFlag() {
        return this.privateFlag;
    }

    public String getPrivateDesc() {
        switch (this.privateFlag) {
            case 0:
                return "仅有偶像";
            case 1:
                return "名人,听众";
            case 2:
                return "所有人";
        }
        return "";
    }

    public String toString() {
        return "PrivateFlag{privateFlag=" + this.privateFlag + '}';
    }
}

